#!/usr/bin/node

//  ___________.__.__         __      __        .__  .__   
//  \_   _____/|__|  |   ____/  \    /  \_____  |  | |  |  
//   |    __)  |  |  | _/ __ \   \/\/   /\__  \ |  | |  |  
//   |     \   |  |  |_\  ___/\        /  / __ \|  |_|  |__
//   \___  /   |__|____/\___  >\__/\  /  (____  /____/____/
//       \/                 \/      \/        \/           


var fs          = require('fs');
var path        = require('path');
var sha         = require('sha');
var param       = process.argv[2];
var fileSystem  = process.argv[3];

var database    = __dirname + '/sha-database.sdb';

var interval    = 1800 * 1000; // 30 minutes

/**
 * FileWall Banner
 */
function banner() {
  console.log("");
  console.log("  ___________.__.__         __      __        .__  .__   ");
  console.log("  \\_   _____/|__|  |   ____/  \\    /  \\_____  |  | |  |  ");
  console.log("   |    __)  |  |  | _/ __ \\   \\/\\/   /\\__  \\ |  | |  |  ");
  console.log("   |     \\   |  |  |_\\  ___/\\        /  / __ \\|  |_|  |__");
  console.log("   \\___  /   |__|____/\\___  >\\__/\\  /  (____  /____/____/");
  console.log("       \\/                 \\/      \\/        \\/           ");
  console.log("");
  console.log("                                        Developed by Enki");
  console.log("=========================================================\n\n");
}

/**
 * Application usage
 */
function usage() {
  console.log("Usage: " + __filename + " [--start] [--cron] [--create-db <directory>] [--add-dir <directory>] [-add-file <file>]");
  process.exit(-1);
}

/**
 * Check file hases for files stored in the database
 */
function checkFileHashes() {
  var LineByLineReader = require('line-by-line'),
    lr = new LineByLineReader(database);

  lr.on('error', function (err) {
    // 'err' contains error object
    console.log("["+new Date()+"] " + err);
  });

  lr.on('line', function (line) {
    // 'line' contains the current line without the trailing newline character.
    var res = line.split(':');
    sha.check(res[0], res[1], function(status){
      if (status){
        var tmpStatus = status.toString().split("at");
        console.log("["+new Date()+"] \n"+tmpStatus[0] + "\n\n");
      }
    });
  });

  lr.on('end', function () {
    // All lines are read, file is closed now.
    console.log("["+new Date()+"] Done checking file system!\n");
  });
}
 
/**
 * Create the database file for storing system files and sha hashs
 */
function createDatabase() {
  walk(fileSystem, function(err, results) {
    //if (err) throw err;
    if (err) console.log("["+new Date()+"] " + err);
  });
}

/**
 * Walk the file system
 */
function walk(dir, done) {
  var results = [];
  fs.readdir(dir, function(err, list) {
    if (err) return done(err);
    var pending = list.length;
    if (!pending) return;
    list.forEach(function(file) {
      file = path.resolve(dir, file);
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function(err, res) {
            if (err) return done(err);
          });
        } else {
          var hash = sha.getSync(file);
          if (hash) {
            var tmp = file + ":" + hash + "\n";
            fs.appendFile(database,tmp, function (err) {
              if (err) console.log("["+new Date()+"] " + err);
            });
          } 
        }
      });
    });
  });
};

/**
 * Start FileWall
 */
function start() {
  console.log("\n["+new Date()+"] "+"Checking if " + database + " exists...");
  if (fs.existsSync(database)) {
    console.log("["+new Date()+"] "+database + " exists! Continuing with scan...\n");
    checkFileHashes();
  } else {
    console.log("["+new Date()+"] "+database + " seems to have been removed? Exiting!");
    process.exit();
  }

  var loop = setInterval(function() {
    console.log("\n["+new Date()+"] "+"Checking if " + database + " exists...");
    if (fs.existsSync(database)) {
      console.log("["+new Date()+"] "+database + " exists! Continuing with scan...\n");
      checkFileHashes();
    } else {
      console.log("["+new Date()+"] "+database + " seems to have been removed? Exiting!");
      process.exit();
    }
  }, interval);
}

/**
 * Running FileWall from cron that is redirecting to a /var/log/ file of choice.
 * Example: 05 * * * * node /some/location/filewall.js --cron >> /var/log/filewall.log
 */
function cron() {
  if (fs.existsSync(database)) {
    checkFileHashes();
  } else {
    console.log("["+new Date()+"] "+database + " seems to have been removed? Exiting!");
    process.exit();
  }
}

/**
 * Read arguments from command line and act accordingly
 */
 if (process.argv.length <= 2) {
  usage();
}

if(param == '--start') {
  banner();
  start();
} else if (param == '--cron') {
  cron();
} else if (param == '--create-db') {
  banner();
  console.log("["+new Date()+"] "+database + " is being refreshed!\n");
  if (fs.existsSync(database)) fs.unlink(database);
  createDatabase();
} else if (param == '--add-dir') {
  banner();
  console.log("["+new Date()+"] "+fileSystem + " is being added to the db!\n");
  createDatabase();
} else if (param == '--add-file') {
  banner();
  console.log("["+new Date()+"] Adding file hash to "+database+" for: "+fileSystem + "\n");
  var hash = sha.getSync(file);
  if (hash) {
    var tmp = fileSystem + ":" + hash + "\n";
    fs.appendFile(database,tmp, function (err) {
      if (err) console.log("["+new Date()+"]"+err);
    });
  }
} else {
  usage();
}

//  ___________.__.__         __      __        .__  .__   
//  \_   _____/|__|  |   ____/  \    /  \_____  |  | |  |  
//   |    __)  |  |  | _/ __ \   \/\/   /\__  \ |  | |  |  
//   |     \   |  |  |_\  ___/\        /  / __ \|  |_|  |__
//   \___  /   |__|____/\___  >\__/\  /  (____  /____/____/
//       \/                 \/      \/        \/           