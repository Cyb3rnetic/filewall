# README

FileWall is a small basic appliation written to help monitor files for changes.
FileWall uses SHA hashes to validate files have not been changed since the initial database creation. 
This is a basic application. Nothing impressive. Quick, to the point, yet...affective. 

# Usage

Below are examples on how-to use FileWall.

Usage: /home/<user>/Node/filewall/filewall.js [--start] [--cron] [--create-db <directory>] [--add-dir <directory>] [-add-file <file>]

## Creating an initial database

[1] ./filewall --create-db ~/Documents

## Adding a directory to an existing database file.

[2] ./filewall --add-dir ~/Music

## Adding a file to an existing database

[3] ./filewall --add-file ~/NewDatabase.kdbx

## Running FileWall

[4] ./filewall --start

## Running from crontab

The log file can be placed where ever you wish. 

When running as a normal user, it is recommend to create the log in a directory readable by the running USER only.

[5] 05 * * * * node /some/location/filewall.js --cron >> /var/log/filewall.log

Notice: If creating a log file in /var/log/ you may need to create /var/log/yourlogfile.log with proper permissions when not running filewall as root.

sudo touch /var/log/filewall.log

sudo chown <user> /var/log/filewall.log

sudo chgrp <user> /var/log/filewall.log

